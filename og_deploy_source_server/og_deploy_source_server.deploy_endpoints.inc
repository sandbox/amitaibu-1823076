<?php
/**
 * @file
 * og_deploy_source_server.deploy_endpoints.inc
 */

/**
 * Implements hook_deploy_endpoints_default().
 */
function og_deploy_source_server_deploy_endpoints_default() {
  $export = array();

  $endpoint = new DeployEndpoint();
  $endpoint->disabled = FALSE; /* Edit this to true to make a default endpoint disabled initially */
  $endpoint->api_version = 1;
  $endpoint->name = 'destination_server';
  $endpoint->title = 'OG Source server';
  $endpoint->description = '';
  $endpoint->debug = 0;
  $endpoint->authenticator_plugin = 'DeployAuthenticatorSession';
  $endpoint->authenticator_config = array(
    'username' => 'admin',
    'password' => '1234',
  );
  $endpoint->service_plugin = 'DeployServiceRestJSON';
  $endpoint->service_config = array(
    'url' => 'http://localhost/drupal715/services/rest',
  );
  $export['destination_server'] = $endpoint;

  return $export;
}
