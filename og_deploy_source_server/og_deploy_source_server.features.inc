<?php
/**
 * @file
 * og_deploy_source_server.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function og_deploy_source_server_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "deploy" && $api == "deploy_endpoints") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "deploy" && $api == "deploy_plans") {
    return array("version" => "1");
  }
}
