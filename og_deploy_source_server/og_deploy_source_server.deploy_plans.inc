<?php
/**
 * @file
 * og_deploy_source_server.deploy_plans.inc
 */

/**
 * Implements hook_deploy_plans_default().
 */
function og_deploy_source_server_deploy_plans_default() {
  $export = array();

  $plan = new DeployPlan();
  $plan->disabled = FALSE; /* Edit this to true to make a default plan disabled initially */
  $plan->api_version = 1;
  $plan->name = 'og_deploy_default';
  $plan->title = 'OG deploy default plan';
  $plan->description = '';
  $plan->debug = 0;
  $plan->aggregator_plugin = 'OgAggregator';
  $plan->aggregator_config = array(
    'gid' => '0',
    'delete_post_deploy' => 0,
    'entities_to_export' => array(
      'Nodes' => 'Nodes',
      'Users' => 'Users',
    ),
  );
  $plan->fetch_only = 0;
  $plan->processor_plugin = 'DeployProcessorBatch';
  $plan->processor_config = array();
  $plan->endpoints = array(
    'destination_server' => 'destination_server',
  );
  $export['og_deploy_default'] = $plan;

  return $export;
}
