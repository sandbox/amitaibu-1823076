


INSTALLATION
------------
== Source server ==

1. Downlowd dependency modules:
   drush dl deploy-2.x-dev services-3.x-dev entity uuid-1.x-dev ctools-1.x-dev  entity_dependency-1.x-dev

2. Apply folloowing patches:
   -Entity dependency - http://drupal.org/node/1545278#comment-6661962
      http://drupal.org/files/entity-dependency-entityreference-1545278-2.patch
   -uuid services - http://drupal.org/node/1694972#comment-6341778
      http://drupal.org/files/uuid-file_entity_fail_deployment-1694972-6.patch
   -uuid - Bypass valid function on vocabulries http://drupal.org/node/1832060
      http://drupal.org/files/uuid_validate_error.patch
   -Deploy - http://drupal.org/node/1785566#comment-6663532
      http://drupal.org/files/entity_label_support_fix.patch
   -og_vocab - http://drupal.org/node/1832324
      http://drupal.org/files/og_vocab_uuid_support-2.patch

3. Temporary solution: make sure you've put spyc.php into the
  /sites/all/modules/contrib/services/servers/rest_server/lib folder,
  [You may or may not have /contrib/ directory] it can be found at
  http://drupal.org/node/1313976#comment-5317796 or http://code.google.com/p/spyc.
  Although this works, it is advised to install the libraries module and place the
  spyc.php file in the sites/all/libraries folder.

4. Download and install og_deploy module
5. Enable module og_deploy_source_server
6. !!!!CLEAR CACHE!!!!
7. Go to admin/structure/deploy/plans/list/og_deploy_default/edit/aggregator
   This is the default plan.
   Select the group node id to export and the type of content, then save.
8. Set credentials of destination server:
   admin/structure/deploy/endpoints/list/destination_server/edit/authenticator
9. set desnitaion server URL:
   admin/structure/deploy/endpoints/list/destination_server/edit/service
10. Go to plan page : admin/structure/deploy, Click deploy



== Destination server

1. Download dependency modules:
   drush dl services-3.x-dev entity uuid-1.x-dev ctools-1.x-dev

2. Apply following patches:
   -Entity dependency - http://drupal.org/node/1545278#comment-6661962
   -uuid services - http://drupal.org/node/1694972#comment-6341778
   -uuid - Bypass valid function on vocabulries http://drupal.org/node/1832060
   -deploy - http://drupal.org/node/1785566
   -og_vocab - http://drupal.org/node/1832324

3. Temporary solution: make sure you've put spyc.php into the
  /sites/all/modules/contrib/services/servers/rest_server/lib folder,
  [You may or may not have /contrib/ directory] it can be found at
  http://drupal.org/node/1313976#comment-5317796 or http://code.google.com/p/spyc.
  Although this works, it is advised to install the libraries module and place the
  spyc.php file in the sites/all/libraries folder.

4. Download and install og_deploy module
5. Enable module og_deploy_destination_server.


More information about deploy installation can be found here:
http://drupal.org/node/1406134
