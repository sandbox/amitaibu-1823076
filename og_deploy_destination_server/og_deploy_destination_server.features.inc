<?php
/**
 * @file
 * og_deploy_destination_server.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function og_deploy_destination_server_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "services" && $api == "services") {
    return array("version" => "3");
  }
}
