<?php

/**
 * Managed deployment aggregator.
 */
class OgAggregator extends DeployAggregatorBase {

  public $config = array();

  /**
   * Holds a reference to the plan object that initiated it.
   *
   * @var DeployPlan
   */
  public $plan;

  public function __construct(DeployPlan $plan = NULL, Array $config = array()) {
    $this->config += array(
      'debug' => FALSE,
      'delete_post_deploy' => FALSE,
      'gid' => 0,
    );
    $this->config = array_merge($this->config, $config);
    $this->plan = $plan;
  }

  /**
   * Get aggregated entities.
   */
  public function getEntities() {
    // Get all the entities that belong to a group.
    if (!$gid = $this->config['gid']) {
      return;
    }
    if (!og_is_group('node', $gid)) {
      return;
    }
    $entities = array();
    // Add the group itself.
    $entities[] = array('type' => 'node', 'id' => $gid);

    // Get Og vocabs and terms to deploy.
    if (module_exists('og_vocab')) {
      // Get all vocabularies related to a group.
      $vocabularies = og_vocab_get_vocabularies_group('node', $gid);
      foreach ($vocabularies as $vocab) {
        // Add vocabulary.
        $entities[] = array('type' => 'taxonomy_vocabulary', 'id' => $vocab->vid);
        // Adding og_vocabs.
        $og_vocabs = og_vocab_get_by_taxonomy_vocabulary($vocab->vid);
        foreach ($og_vocabs as $id) {
          $entities[] = array('type' => 'og_vocab', 'id' => $id);
        }

        // Add terms.
        $terms = taxonomy_get_tree($vocab->vid);
        foreach ($terms as $term) {
          $entities[] = array('type' => 'taxonomy_term', 'id' => $term->tid);
        }
      }
    }
    // Get group content and group members.
    $group_type = array();
    foreach ($this->config['entities_to_export'] as $entity_type => $enable) {
      if ($enable) {
        $group_type[] = $entity_type;
      }
    }

    if (!empty($group_type)) {
      $query = new EntityFieldQuery();
      $result = $query
        ->entityCondition('entity_type', 'og_membership')
        ->propertyCondition('group_type', $group_type, 'IN')
        ->propertyCondition('gid', $gid)
        ->execute();

      if (empty($result['og_membership'])) {
        return;
      }

      $og_memberships = og_membership_load_multiple(array_keys($result['og_membership']));

      foreach ($og_memberships as $og_membership) {
        $entities[] = array('type' => $og_membership->entity_type, 'id' => $og_membership->etid);
      }
    }
    return $entities;
  }

  public function configForm(&$form_state) {
    $form = array();
    $form['gid'] = array(
      '#type' => 'textfield',
      '#title' => t('Group ID'),
      '#description' => t('You can search group by name.'),
      '#default_value' => $this->config['gid'],
      '#autocomplete_path' => 'og/autocomplete',
    );

    $form['delete_post_deploy'] = array(
      '#type' => 'checkbox',
      '#title' => t('Delete successfully deployed entities from the plan'),
      '#description' => t('If checked, each successfully deployed entity will be automatically deleted from the plan.'),
      '#default_value' => $this->config['delete_post_deploy'],
    );

    $form['entities_to_export'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Entities to export'),
      '#options' => array('node' => t('Nodes'), 'user' => t('Users'), ),
      '#default_value' => $this->config['entities_to_export'],
    );
    return $form;
  }
}
